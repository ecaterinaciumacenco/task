#!/bin/bash
# Spin up the box and Update all the packages
   yum update -y

# Configure a new repository to postgress
   yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm

# Install postgress
   yum install -y postgresql13-server

# Iniciate database
   /usr/pgsql-13/bin/postgresql-13-setup initdb

# Start and enable postgress
   systemctl enable postgresql-13 --now

# Create database and user and give privelages
  sudo -u postgres psql -c "CREATE DATABASE confluence;"
  sudo -u postgres psql -c "CREATE USER confluence WITH PASSWORD 'password';"
  sudo -u postgres psql -c "GRANT ALL ON DATABASE confluence TO confluence;"

# Adding firewall roles
  firewall-cmd --permanent --add-port=5432/tcp 
  firewall-cmd --permanent --add-service=postgresql
  firewall-cmd --reload

# Enable client authentication
  echo "host    all     all     192.168.1.0/24 trust" >> /var/lib/pgsql/13/data/postgresql.conf

# Allow TCP/IP socket 
    sed -i 's/localhost/*/g'  /var/lib/pgsql/13/data/postgresql.conf

# Restart database
    systemctl restart postgresql-13
