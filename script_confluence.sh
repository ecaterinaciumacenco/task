#!/bin/bash
# Spin up the box and Update all the packages
   yum update -y

# Install java package
   yum install java-11-openjdk-devel
   yum install java-1.8.0-openjdk

# Create instalation directory
   mkdir /confluence

# Move dowloaded archive to instalation directory
   mv atlassian-confluence-7.12.4.tar.gz /confluence/

# Create user with password
   useradd confluence
   passwd confluence

# Give your dedicated Confluence user read, write and execute permission
   cd /
   chmod -R u=rwx,go-rwx confluence/
   chown -R confluence confluence/

# Change in to instalation directory and untar the archive
   cd confluence/
   tar xzf atlassian-confluence-7.12.4.tar.gz
  
# Create the home directory
   cd /
   mkdir confluence-home

# Give your dedicated Confluence user read, write and execute permissions to the <home-directory>.
   chown -R confluence confluence-home/
   chmod -R u=rwx,go-rwx confluence-home/

# Assign confluence.home verible full path to working directory
    echo "confluence.home=/confluence-home/" >> confluence/atlassian-confluence-7.12.4/confluence/WEB-INF/classes/confluence-init.properties

# Start confluence server
    cd confluence/atlassian-confluence-7.12.4/bin/
    ./start-confluence.sh

# Add port to firewall
    firewall-cmd --add-port=8090/tcp --permanent
    firewall-cmd --reload
