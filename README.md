
#### MANUAL STEPS ###
### DATABASE ###

1. Spin up the box and Update all the packages
   ```
   yum update -y 
   ```

2. Configure a new repository to postgress
   ```
   yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
   ```

3. Install postgress
   ```
   yum install -y postgresql13-server
   ```
   
4. Initiate database
   ```
   /usr/pgsql-13/bin/postgresql-13-setup initdb
   ```
   
5. Start and enable postgress
   ```
   systemctl enable postgresql-13 --now
   ```

6. Create database and user and give privelages 
   ```
   sudo -u postgres psql -c "CREATE DATABASE confluence;"
   ```
   ```
   sudo -u postgres psql -c "CREATE USER confluence WITH PASSWORD 'password';"
   ```
   ```
   sudo -u postgres psql -c "GRANT ALL ON DATABASE confluence TO confluence;"
   ```
   
7. Adding firewall roles
   ```
   firewall-cmd --permanent --add-port=5432/tcp 
   ```
   ```
   firewall-cmd --permanent --add-service=postgresql
   ```
   ```
   firewall-cmd --reload
   ```
   
8. Enable client authentication
   ```
   vi /var/lib/pgsql/13/data/postgresql.conf
   # Adding in to the end of the file:
      host    all     all     192.168.1.0/24 trust
   ```
   
9. Allow TCP/IP socket 
   ```
   vi /var/lib/pgsql/13/data/postgresql.conf
   # uncoment line 60 and instead of 'localhost' add '*'
   ```
   
10. Restert database
    ```
    systemctl restart postgresql-13
    ```

### CONFLUENCE  ###

1. Spin up the box and Update all the packages
   ```
   yum update -y
   ```
   
2. Install java package
   ```
   yum install java-11-openjdk-devel
   ```
   ```
   yum install java-1.8.0-openjdk
   ```
   ```
   java -version
   ```
   
3. Create instalation directory
   ```
   mkdir /confluence
   ```
   
4. Move dowloaded archive to instalation directory
   ```
   mv atlassian-confluence-7.12.4.tar.gz /confluence/
   ```
   
5. Create user with password
   ```
   useradd confluence
   ```
   ```
   passwd confluence
   ```
   
6. Give your dedicated Confluence user read, write and execute permission
   ```
   cd /
   ```
   ```
   chmod -R u=rwx,go-rwx confluence/
   ```
   ```
   chown -R confluence confluence/
   ```
   
7. Change in to instalation directory and untar the archive
   ```
   cd confluence/
   ```
   ```
   tar xzf atlassian-confluence-7.12.4.tar.gz
   ```
   
8. Create the home directory
   ```
   cd /
   ```
   ```
   mkdir confluence-home
   ```
   
9. Give your dedicated Confluence user read, write and execute permissions to the <home-directory>.
   ```
   chown -R confluence confluence-home/
   ```
   ```
   chmod -R u=rwx,go-rwx confluence-home/
   ```
   
10. Assign confluence.home verible full path to working directory
   ```
   vi confluence/atlassian-confluence-7.12.4/confluence/WEB-INF/classes/confluence-init.properties
   # Line 26, uncoment
   confluence.home=/confluence-home/
   ```
   
11. Start confluence server
    ```
    cd confluence/atlassian-confluence-7.12.4/bin/
    ```
    ```
    ./start-confluence.sh
    ```
    
12. Add port to firewall
    ```
    firewall-cmd --add-port=8090/tcp --permanent
    ```
    ```
    firewall-cmd --reload
    ```